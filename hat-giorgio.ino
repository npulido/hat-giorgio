// Paint example specifically for the TFTLCD breakout board.
// If using the Arduino shield, use the tftpaint_shield.pde sketch instead!
// DOES NOT CURRENTLY WORK ON ARDUINO LEONARDO
//Technical support:goodtft@163.com

#include <Elegoo_GFX.h>    // Core graphics library
#include <Elegoo_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>
#include <math.h>

#if defined(__SAM3X8E__)
    #undef __FlashStringHelper::F(string_literal)
    #define F(string_literal) string_literal
#endif

// When using the BREAKOUT BOARD only, use these 8 data lines to the LCD:
// For the Arduino Uno, Duemilanove, Diecimila, etc.:
//   D0 connects to digital pin 8  (Notice these are
//   D1 connects to digital pin 9   NOT in order!)
//   D2 connects to digital pin 2
//   D3 connects to digital pin 3
//   D4 connects to digital pin 4
//   D5 connects to digital pin 5
//   D6 connects to digital pin 6
//   D7 connects to digital pin 7

// For the Arduino Mega, use digital pins 22 through 29
// (on the 2-row header at the end of the board).
//   D0 connects to digital pin 22
//   D1 connects to digital pin 23
//   D2 connects to digital pin 24
//   D3 connects to digital pin 25
//   D4 connects to digital pin 26
//   D5 connects to digital pin 27
//   D6 connects to digital pin 28
//   D7 connects to digital pin 29

// For the Arduino Due, use digital pins 33 through 40
// (on the 2-row header at the end of the board).
//   D0 connects to digital pin 33
//   D1 connects to digital pin 34
//   D2 connects to digital pin 35
//   D3 connects to digital pin 36
//   D4 connects to digital pin 37
//   D5 connects to digital pin 38
//   D6 connects to digital pin 39
//   D7 connects to digital pin 40
/*
#define YP 9  // must be an analog pin, use "An" notation!
#define XM 8  // must be an analog pin, use "An" notation!
#define YM A2   // can be a digital pin
#define XP A3   // can be a digital pin
*/

#define YP A3  // must be an analog pin, use "An" notation!
#define XM A2  // must be an analog pin, use "An" notation!
#define YM 9   // can be a digital pin
#define XP 8   // can be a digital pin
/*
#define TS_MINX 50
#define TS_MAXX 920

#define TS_MINY 100
#define TS_MAXY 940
*/
//Touch For New ILI9341 TP
#define TS_MINX 120
#define TS_MAXX 900

#define TS_MINY 70
#define TS_MAXY 920

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
// optional
#define LCD_RESET A4

// Assign human-readable names to some common 16-bit color values:
#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF


Elegoo_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

#define BOXSIZE 40
#define PENRADIUS 3
int oldcolor, currentcolor;

void setup(void) {
  Serial.begin(9600);
  Serial.println(F("Paint!"));
  
  tft.reset();
  
  uint16_t identifier = tft.readID();
  if(identifier == 0x9325) {
    Serial.println(F("Found ILI9325 LCD driver"));
  } else if(identifier == 0x9328) {
    Serial.println(F("Found ILI9328 LCD driver"));
  } else if(identifier == 0x4535) {
    Serial.println(F("Found LGDP4535 LCD driver"));
  }else if(identifier == 0x7575) {
    Serial.println(F("Found HX8347G LCD driver"));
  } else if(identifier == 0x9341) {
    Serial.println(F("Found ILI9341 LCD driver"));
  } else if(identifier == 0x8357) {
    Serial.println(F("Found HX8357D LCD driver"));
  } else if(identifier==0x0101)
  {     
      identifier=0x9341;
       Serial.println(F("Found 0x9341 LCD driver"));
  }else {
    Serial.print(F("Unknown LCD driver chip: "));
    Serial.println(identifier, HEX);
    Serial.println(F("If using the Elegoo 2.8\" TFT Arduino shield, the line:"));
    Serial.println(F("  #define USE_Elegoo_SHIELD_PINOUT"));
    Serial.println(F("should appear in the library header (Elegoo_TFT.h)."));
    Serial.println(F("If using the breakout board, it should NOT be #defined!"));
    Serial.println(F("Also if using the breakout, double-check that all wiring"));
    Serial.println(F("matches the tutorial."));
    identifier=0x9341;
   
  }

  tft.begin(identifier);
  tft.setRotation(2);
  tft.fillScreen(BLACK);
  currentcolor = RED;

  pinMode(13, OUTPUT);  // For the touchscreen.
  pinMode(30, INPUT_PULLUP);
  pinMode(31, INPUT_PULLUP);
  pinMode(32, INPUT_PULLUP);
  pinMode(33, INPUT_PULLUP);
  pinMode(34, INPUT_PULLUP);

  int random_seed = analogRead(15);
  randomSeed(random_seed);
  Serial.print("Random seed: "); Serial.println(random_seed);
}


// Status:
int ions = 0;

int coils = 0;
int lf_com = 0;
int lf_rock = 0;
int sh_hor = 0;
int tickle = 0;
int shaped = 0;

float fidelity = 681;
bool game_over = false;



void print_status(void) {
  Serial.println("Status:");
  Serial.print("ions: "); Serial.println(ions);
  Serial.print("coils: "); Serial.println(coils);
  Serial.print("lf_com: "); Serial.println(lf_com);
  Serial.print("lf_rock: "); Serial.println(lf_rock);
  Serial.print("sh_hor: "); Serial.println(sh_hor);
  Serial.print("tickle: "); Serial.println(tickle);
  Serial.print("fidelity: "); Serial.println(fidelity);
  Serial.print("shaped: "); Serial.println(shaped);
  Serial.println();
}


void get_fidelity(void) {
  bool all_two = false;
  if ((coils > 0) &&
     (lf_com > 0) &&
    (lf_rock > 0) &&
     (sh_hor > 0) &&
     (tickle > 0) &&
     (shaped > 0)
  ) {
    fidelity = 863;
  }
  if ((coils >= 2) &&
    (lf_com >= 2) &&
   (lf_rock >= 2) &&
    (sh_hor >= 2) &&
    (tickle >= 2) &&
    (shaped >= 2)
  ) {
    fidelity = 982;
    all_two = true;
  }
  if (((coils > 2) ||
     (lf_com > 2) ||
    (lf_rock > 2) ||
     (sh_hor > 2) ||
     (tickle > 2) ||
    (shaped > 2)
  ) && all_two == true) {
    fidelity = 997;
  }
}


// Some program parameters
int datapoint_delay = 100; 
int lose_probability = 1;



#define MINPRESSURE 10
#define MAXPRESSURE 1000
void pulse_shaper() {
  tft.fillScreen(BLACK);
  tft.setRotation(1);
  tft.setCursor(0,0);
  tft.setTextColor(WHITE);
  tft.setTextSize(4);
  tft.print("Pulse shaper");
  tft.setRotation(2);

  while (true) {
    digitalWrite(13, HIGH);
    TSPoint p = ts.getPoint();
    digitalWrite(13, LOW);

    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);

    // points
    int px0 = 100;
    int px1 = 125;
    int py0 = 70;
    int py1 = 110;
    int py2 = 210;
    int py3 = 250;

    if (false) {  // debug
      tft.fillCircle(px0, py3, 3, CYAN);
      tft.fillCircle(px0, py0, 3, CYAN);
      tft.fillCircle(px1, py2, 3, CYAN);
      tft.fillCircle(px1, py1, 3, CYAN);
    }

    if (p.z > MINPRESSURE && p.z < MAXPRESSURE) {
      p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0);
      p.y = (tft.height()-map(p.y, TS_MINY, TS_MAXY, tft.height(), 0));
      tft.fillCircle(p.x, p.y, PENRADIUS, currentcolor);
      Serial.print("p: ");Serial.print(p.x);Serial.print(" ");Serial.print(p.y);Serial.print("\n");
      if ( ((p.x < px0) && (p.y > py3)) || 
          ((p.x < px0) && (p.y < py0)) || 
          ((p.x > px1) && (p.y < py2) && (p.y > py1)) ) {
        
        tft.fillScreen(BLACK);
        tft.setRotation(1);
        tft.setCursor(0,0);
        tft.setTextColor(WHITE);
        tft.setTextSize(4);
        tft.print("Bad pulse...\n");
        delay(1000);
        tft.print("\nPDQ Fail!\n");
        delay(1000);
        tft.print("\n\nTry again...\n");
        delay(1000);
        pulse_shaper();
        return;
      }
      else if (p.y < 30) {
        shaped++;
        tft.setRotation(1);
        tft.setCursor(0,0);
        tft.print("\n\n\n\n");
        tft.setTextColor(GREEN);
        tft.setTextSize(4);
        tft.print("Good pulse!\n");
        delay(5000);
        pulse_shaper();
        return;
      }
    }
    
    if ( (digitalRead(30) == LOW) || (digitalRead(31) == LOW) || (digitalRead(32) == LOW) || (digitalRead(33) == LOW) ) {
      tft.fillScreen(BLACK);
      break;
    }
  }
}


// https://gist.github.com/nadavmatalon/71ccaf154bc4bd71f811289e78c65918
double mapf(double val, double in_min, double in_max, double out_min, double out_max) {
    return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


void showln(char *s, uint8_t size = 2, uint16_t color = WHITE) {
  tft.setTextColor(color);
  tft.setTextSize(size);
  tft.setRotation(1);
  tft.println(s);
}


void show(char *s, uint8_t size = 2, uint16_t color = WHITE) {
  tft.setTextColor(color);
  tft.setTextSize(size);
  tft.setRotation(1);
  tft.print(s);
}


void lose_ion(int probabilty, bool all = false, bool verbose = false) {
  long roll = random(101);
  if ((roll < probabilty) && (ions > 0)) {
    if (all == true) ions = 0;
    else ions--;
  }
  if (verbose == true) { 
    Serial.print("lose_ion roll: "); Serial.print(roll);
    Serial.print("\nions left: "); Serial.println(ions);
  }
}


void trap() {
  Serial.println("Its a Trap!");
  show_ions();
  while (true) {
    if (digitalRead(34) == LOW) {
      ions = (ions + 1) % 3;
      lose_ion(25);
      trap();
      return; 
    }
    else if ((digitalRead(30) == LOW) || (digitalRead(31) == LOW) || 
    (digitalRead(32) == LOW) || (digitalRead(33) == LOW)) {
      return;
    }
  }
}


void show_ions() {
  if (ions == 1) {
    tft.setRotation(1);
    tft.fillScreen(BLACK);
    tft.setCursor(0, 0);
    tft.setTextColor(WHITE);

    tft.setTextSize(4);
    tft.println("1 ION");

    tft.setTextSize(1);
    //tft.print("       `                    ``      `            ..  ");
    //tft.print("    `    `             `   `          `.        ` `  ");
    //tft.print("    ``:`         .``                      .``` `     ");
    //tft.print("`  .` ``     `  `   `` .             `` `       ` -  ");
    //tft.print("           .         ` `  `        ```         ` `   ");
    //tft.print("    .  -        `  .`               `  `             ");
    tft.print("       `                    ``      `            ..  ");
    tft.print("` `   `      .        ` ``    ` `      ` `   `    `  ");
    tft.print("`  .` ``     `  `   `` .             `` `       ` -  ");
    tft.print("           .         ` `  `        ```         ` `   ");
    tft.print("` `   `      .        ` ``    ` `      ` `   `    `  ");
    tft.print("           ``    `.. ``   .    ` .``        ``     ``");
    tft.print("     /     `  ``` ``      `      .    ` .`           ");
    tft.print("     `  .    -`   .  `:++.. .    .:   ``-    `   `   ");
    tft.print("     `     `    .` .`oNMMmy:`    ..  `   .`  ``      ");
    tft.print("    .   `      `` ` .:MMMMMNy::-``.     - ```        ");
    tft.print(" `  `  `   ``  `- . -.dMmhdmdmd/``          `        ");
    tft.print(" `  -  ` `.`    `   ``yMdyyyyydho.:.`     `      `  `");
    tft.print("    `              ` .dhy+:s+::+/``-`     .```  `. ``");
    tft.print("` `       .`         +d+-:.--:::+. .```     .``  .   ");
    tft.print("`    `.   .         .hs-`..-:-`..`-` ..`        ``   ");
    tft.print("  `               ``-h:`-`:-```  `/`.`-..           `");
    tft.print("        `  `-` `````-h-..`.o  ..``..-`. +        ` ``");
    tft.print("      `      `  `  --.:`-`-.  .`.`/`.   ``  ``   -`. ");
    tft.print("     `.   `  ` `` `-..:...-: `.   `  ```:-  -` ``-.-`");
    tft.print("`     .` `   `    .:/ `.``-` `/   ` `   .     ` `  ``");
    tft.print("`  ``  `   `       .. ``.`.```:`` ``       ``        ");
    tft.print("       `        ` `  ..``.`-.--`-. -. `      .- ``` `");
    //tft.print("`  `    `       ``    ```-`- `.``` /-        `.      ");
    //tft.print("   -.  ` `          .- ``  -`````` `.``  ``    .     ");

    tft.setTextSize(2);
    tft.println("press F5 to trap more or\nanother key to continue...");
    return;
  }
  if (ions == 2) {

    tft.setRotation(1);
    tft.fillScreen(BLACK);
    tft.setCursor(0, 0);
    tft.setTextColor(WHITE);

    tft.setTextSize(4);
    tft.println("2 IONS");

    tft.setTextSize(1);

    tft.print("       `                    ``      `            ..  ");
    tft.print("` `   `      .        ` ``    ` `      ` `   `    `  ");
    tft.print("`  .` ``     `  `   `` .             `` `       ` -  ");
    tft.print("           .         ` `  `        ```         ` `   ");
    tft.print("` `   `      .        ` ``    ` `      ` `   `    `  ");
    tft.print("           ``    `.. ``   .    ` .``        ``     ``");
    tft.print("     /     `  ``` ``      `      .    ` .`           ");
    tft.print("    `  .    -`   .  `:++..  `:++.. .    .:   ``-     ");
    tft.print("    `     `    .` .`oNMMmy:`oNMMmy:`    ..  `   .`   ");
    tft.print("   .   `      `` ` .:MMMMMN.:MMMMMNy::-``.     - ``` ");
    tft.print("   `  `   ``  `- . -.dMmhdm-.dMmhdmdmd/``          ` ");
    tft.print("   -  ` `.`    `   ``yMdyyy``yMdyyyyydho.:.`     `  `");
    tft.print("   `              ` .dhy+:s .dhy+:s+::+/``-`     .```");
    tft.print("``       .`         +d+-:.- +d+-:.--:::+. .```     . ");
    tft.print("`   `.   .         .hs-`..-.hs-`..-:-`..`-` ..`      ");
    tft.print(" `               ``-h:`-`:--h:`-`:-```  `/`.`-..    `");
    tft.print("       `  `-` `````-h-..`.o-h-..`.o  ..``..-`. +    `");
    tft.print("     `      `  `  --.:`-`-.-.:`-`-.  .`.`/`.   ``  ` ");
    tft.print("    `.   `  ` `` `-..:...-:..:...-: `.   `  ```:-  -`");
    tft.print("`    .` `   `    .:/ `.``-`/ `.``-` `/   ` `   .    `");
    tft.print("` ``  `   `       .. ``.`.`. ``.`.```:`` ``       `` ");
    tft.print("      `        ` `  ..``.`- ..``.`-.--`-. -. `      `");

    tft.setTextSize(2);
    tft.println("press F5 to trap more or\nanother key to continue...");
    return;
  }
  if (ions == 0) {

    tft.setRotation(1);
    tft.fillScreen(BLACK);
    tft.setCursor(0, 0);
    tft.setTextColor(WHITE);

    tft.setTextSize(4);
    tft.println("0 IONS");

    tft.setTextSize(1);
    tft.print("       `                    ``      `            ..  ");
    tft.print("    `    `             `   `          `.        ` `  ");
    tft.print("    ``:`         .``                      .``` `     ");
    tft.print("`  .` ``     `  `   `` .             `` `       ` -  ");
    tft.print("           .         ` `  `        ```         ` `   ");
    tft.print("    .  -        `  .`               `  `             ");
    tft.print("       `                    ``      `            ..  ");
    tft.print("` `   `      .        ` ``    ` `      ` `   `    `  ");
    tft.print("`  .` ``     `  `   `` .             `` `       ` -  ");
    tft.print("           .         ` `  `        ```         ` `   ");
    tft.print("` `   `      .        ` ``    ` `      ` `   `    `  ");
    tft.print("           ``    `.. ``   .    ` .``        ``     ``");
    tft.print("   `       `  ``` ``      `      .    ` .`           ");
    tft.print("     `  .    -`     ``-    `   `   ` `             ` ");
    tft.print("     `     `    .` `   .`  ``               `     ` `");
    tft.print("    .   `      ``     - ```         `                ");
    tft.print(" `  `  `   ``  `-         `                         .");
    tft.print(" `  -  ` `.`    ` `     `      `  ` ` `   `     `    ");
    tft.print("    `             `     .```  `. ``` `             ` ");
    tft.print("` `       .`      ```     .``  .            `     ` `");
    tft.print("`    `.   .        ..`        ``    ` `   `     `    ");

    tft.setTextSize(2);
    tft.println("press F5 to trap more or\nanother key to continue...");
    return;
  }
}


float sinc2(float x) {
  if (x == 0) return 1;
  else return (sin(x) / x) * (sin(x) / x); 
}


float x2m(float x) {
  return 1 - x*x;
}


float sin2(float x) {
  return sin(x)*sin(x);
}


void compensation_coils(void) {
  tft.setRotation(1);
  tft.setCursor(0,0);
  tft.fillScreen(WHITE);
  int points = 40;  // Number of datapoints
  float modifier = 10;  // Modifier for the fit
  float phi_min = -3.1416*3;
  float phi_max = 3.1416*3;
  float a = (random(101) / 10.) - 5;  // displace x (randomly)
  Serial.print("x displ "); Serial.println(a);
  float b0 = 0.3;  // displace y
  float c = 1;  // strecth x
  float d = 0.7;  // stretch y
  float step = (phi_max - phi_min) / points;
  float x, y, new_x, new_y;
  int margin = 10;

  // Frame
  tft.drawRect(margin, margin, tft.width() - 2*margin, tft.height() - 2*margin, BLACK);

  // Plot
  for (int i = 0; i < points; i++) {
    lose_ion(lose_probability);
    x = phi_min + i*step;
    y = d*(sinc2(c*(x-a)) + b0);
    if (ions == 0) y = (1+b0)*d;
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 3, BLACK);
    if (ions != 0) delay(datapoint_delay);
  }

  tft.setCursor(margin, margin);
  tft.setTextColor(BLACK);
  tft.setTextSize(3);
  if (ions == 0) {
    tft.print("Can't set coils.");
    if (ions == 0) y = 1;
  }
  else {
    tft.print("Coil current set!");
    coils++;
  }

  while (true) {
    if ((digitalRead(30) == LOW) || (digitalRead(31) == LOW) || (digitalRead(32) == LOW) || (digitalRead(33) == LOW)) {
      return;
    }
  }
}


void raman_lf_com(void) {
  tft.setRotation(1);
  tft.setCursor(0,0);
  tft.fillScreen(WHITE);
  int points = 40;  // Number of datapoints
  float modifier = 10;  // Modifier for the fit
  float phi_min = -3.1416*3;
  float phi_max = 3.1416*3;
  float a = (random(101) / 10.) - 5;  // displace x (randomly)
  Serial.print("x displ "); Serial.println(a);
  float b0 = 0.3;  // displace y
  float c = 0.5;  // strecth x
  float d = 0.6;  // stretch y
  float step = (phi_max - phi_min) / points;
  float x, y, new_x, new_y;
  int margin = 10;

  // Frame
  tft.drawRect(margin, margin, tft.width() - 2*margin, tft.height() - 2*margin, BLACK);

  // Plot
  for (int i = 0; i < points; i++) {
    lose_ion(lose_probability);  // Lose an ion
    x = phi_min + i*step;
    y = d*(sinc2(c*(x-a)) + b0);
    if (ions == 0) y = (1+b0)*d;
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 3, BLACK);
    if (ions != 0) delay(datapoint_delay);
  }

  // Fit
  for (int i = 0; i < points*modifier; i++) {
    if (ions == 0) break;
    x = phi_min + i*step/modifier;
    y = d*(sinc2(c*(x-a)) + b0);
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 1, GREEN);
  }

  tft.setCursor(margin, margin);
  tft.setTextColor(BLACK);
  tft.setTextSize(3);
  if (ions == 0) {
    tft.print("Fit failed!");
  }
  else {
    tft.print("LF COM found!");
    lf_com++;
  }

  while (true) {
    if ((digitalRead(30) == LOW) || (digitalRead(31) == LOW) || (digitalRead(32) == LOW) || (digitalRead(33) == LOW)) {
      return;
    }
  }
}


void raman_lf_rock(void) {
  tft.setRotation(1);
  tft.setCursor(0,0);
  tft.fillScreen(WHITE);
  int points = 40;  // Number of datapoints
  float modifier = 10;  // Modifier for the fit
  float phi_min = -3.1416*3;
  float phi_max = 3.1416*3;
  float a = (random(101) / 10.) - 5;  // displace x (randomly)
  Serial.print("x displ "); Serial.println(a);
  float b0 = 0.3;  // displace y
  float c = 0.6;  // strecth x
  float d = 0.5;  // stretch y
  float step = (phi_max - phi_min) / points;
  float x, y, new_x, new_y;
  int margin = 10;

  // Frame
  tft.drawRect(margin, margin, tft.width() - 2*margin, tft.height() - 2*margin, BLACK);

  // Plot
  for (int i = 0; i < points; i++) {
    lose_ion(lose_probability);  // Lose an ion 
    x = phi_min + i*step;
    y = d*(sinc2(c*(x-a)) + b0);
    if (ions == 0) y = (1+b0)*d;
    if (ions == 1) y = (0.5+b0)*d;
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 3, BLACK);
    if (ions == 2) delay(datapoint_delay);
  }

  // Fit
  for (int i = 0; i < points*modifier; i++) {
    if (ions < 2) break;
    x = phi_min + i*step/modifier;
    y = d*(sinc2(c*(x-a)) + b0);
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 1, GREEN);
  }

  tft.setCursor(margin, margin);
  tft.setTextColor(BLACK);
  tft.setTextSize(3);
  if (ions < 2) {
    tft.print("Fit failed!");
  }
  else {
    tft.print("LF Rock found!");
    lf_rock++;
  }

  while (true) {
    if ((digitalRead(30) == LOW) || (digitalRead(31) == LOW) || (digitalRead(32) == LOW) || (digitalRead(33) == LOW)) {
      return;
    }
  }
}


void aczs(void) {
  tft.setRotation(1);
  tft.setCursor(0,0);
  tft.fillScreen(WHITE);
  int points = 40;  // Number of datapoints
  float modifier = 10;  // Modifier for the fit

  float phi_min = -1;
  float phi_max = 1;
  float a = (random(6) / 10.)-0.5;  // displace x (randomly)
  float b0 = 0.1;  // displace y
  float c = 1;  // strecth x
  float d = 0.7;  // stretch y

  float step = (phi_max - phi_min) / points;
  float x, y, new_x, new_y;
  int margin = 10;

  Serial.print("x displ "); Serial.println(a);

  // Frame
  tft.drawRect(margin, margin, tft.width() - 2*margin, tft.height() - 2*margin, BLACK);

  // Plot
  for (int i = 0; i < points; i++) {
    lose_ion(lose_probability);  // Lose an ion
    x = phi_min + i*step;
    y = d*(x2m(c*(x-a)) + b0);
    if (ions == 0) y = (1+b0)*d;
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 3, BLACK);
    if (ions != 0) delay(datapoint_delay);
  }

  // Fit
  for (int i = 0; i < points*modifier; i++) {
    if (ions < 1) break;
    x = phi_min + i*step/modifier;
    y = d*(x2m(c*(x-a)) + b0);
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 1, GREEN);
  }

  tft.setCursor(margin, margin);
  tft.setTextColor(BLACK);
  tft.setTextSize(3);
  if (ions < 1) {
    tft.print("Fit failed!");
  }
  else {
    tft.print("Shims calibrated!");
    sh_hor++;
  }

  while (true) {
    if ((digitalRead(30) == LOW) || (digitalRead(31) == LOW) || (digitalRead(32) == LOW) || (digitalRead(33) == LOW)) {
      return;
    }
  }
}


void gate(void) {
  tft.setRotation(3);  //  <-- Plot Rotation!
  tft.setCursor(0,0);
  tft.fillScreen(WHITE);
  int points = 40;  // Number of datapoints
  float modifier = 10;  // Modifier for the fit

  float phi_min = -3.1416;
  float phi_max = 3.1416;
  float a = ((random(6) / 10.)-0.5)*0;  // displace x (randomly)
  float a2 = 3.1416 / 2;
  float b0 = 0.01;  // displace y
  float b1 = 0.05;  // displace y
  float b2 = 0.01;  // displace y
  float c = 1;  // strecth x
  float d0 = 0.47;   // stretch y
  float d1 = 0.49;   // stretch y
  float d2 = 0.98;   // stretch y
  
  float step = (phi_max - phi_min) / points;
  float x, x2, y0, y1, y2, new_x, new_y0, new_y1, new_y2;
  int margin = 10;


  // Fidelity modifier
  float new_margin = ( tft.height() - (tft.height() - 2*margin)*(fidelity/1000.0) ) / 2;

  Serial.print("x displ "); Serial.println(a);

  // Frame
  tft.drawRect(margin, margin, tft.width() - 2*margin, tft.height() - 2*margin, BLACK);

  // Plot
  for (int i = 0; i < points; i++) {
    lose_ion(lose_probability);  // Lose an ion
    //ions = 2; // debug
    x = phi_min + i*step;
    y0 = d0*(sin2(c*(x-a)) + b0);
    y1 = d1*(sin2(c*(x-a)) + b1);
    y2 = d2*(sin2(c*(x-a2)) + b2);
    if (ions == 0) y0 = (1+b0)*d0;
    if (ions == 0) y1 = (1+b1)*d1;
    if (ions == 0) y2 = (1+b2)*d2;
    if (ions == 1) y0 = (0.5+b0)*d0;
    if (ions == 1) y1 = (0.5+b1)*d1;
    if (ions == 1) y2 = (0.5+b2)*d2;
    new_x = mapf(x, phi_min, phi_max, tft.width() -margin,  0 +margin);
    new_y0 = mapf(y0, 0, 1, 0 +new_margin, tft.height() -new_margin);
    new_y1 = mapf(y1, 0, 1, 0 +new_margin, tft.height() -new_margin);
    new_y2 = mapf(y2, 0, 1, 0 +new_margin, tft.height() -new_margin);
    tft.fillCircle(new_x, new_y0, 3, RED);
    tft.fillCircle(new_x, new_y1, 3, BLUE);
    tft.fillCircle(new_x, new_y2, 3, GREEN);
    if (ions == 2) delay(datapoint_delay);
  }

  // Fit
  for (int i = 0; i < points*modifier; i++) {
    if (ions < 2) break;
    x = phi_min + i*step/modifier;
    y0 = d0*(sin2(c*(x-a)) + b0);
    y1 = d1*(sin2(c*(x-a)) + b1);
    y2 = d2*(sin2(c*(x-a2)) + b2);
    new_x = mapf(x, phi_min, phi_max, tft.width() -margin, 0 +margin);
    new_y0 = mapf(y0, 0, 1, 0 +new_margin, tft.height() -new_margin);
    new_y1 = mapf(y1, 0, 1, 0 +new_margin, tft.height() -new_margin);
    new_y2 = mapf(y2, 0, 1, 0 +new_margin, tft.height() -new_margin);
    tft.fillCircle(new_x, new_y0, 1, RED);
    tft.fillCircle(new_x, new_y1, 1, BLUE);
    tft.fillCircle(new_x, new_y2, 1, GREEN);
  }

  tft.setRotation(1);
  tft.setCursor(margin, margin);
  tft.setTextColor(BLACK);
  tft.setTextSize(3);
  if (ions < 2) {
    tft.print("Ion lost!");
  }
  else {
    tft.print("F = "); tft.print(fidelity/10.0); tft.print("%");
    if (fidelity >= 996) {
      game_over = true;
    }
  }

  while (true) {
    if ((digitalRead(30) == LOW) || (digitalRead(31) == LOW) || (digitalRead(32) == LOW) || (digitalRead(33) == LOW)) {
      return;
    }
  }
}


void tickle_raman(void) {
  tft.setRotation(1);
  tft.setCursor(0,0);
  tft.fillScreen(WHITE);
  int points = 40;  // Number of datapoints
  float modifier = 10;  // Modifier for the fit
  float phi_min = -3.1416*3;
  float phi_max = 3.1416*3;
  float a = (random(101) / 10.) - 5;  // displace x (randomly)
  Serial.print("x displ "); Serial.println(a);
  float b0 = 1.2;  // displace y
  float c = 1.2;  // strecth x
  float d = 0.2;  // stretch y
  float step = (phi_max - phi_min) / points;
  float x, y, new_x, new_y;
  int margin = 10;

  // Frame
  tft.drawRect(margin, margin, tft.width() - 2*margin, tft.height() - 2*margin, BLACK);

  tft.setCursor(margin, margin);
  tft.setTextColor(BLACK);
  tft.setTextSize(3);
  tft.println("Tickle the Raman!");

  // Wait for pressure
  while (true) {
    digitalWrite(13, HIGH);
    TSPoint p = ts.getPoint();
    digitalWrite(13, LOW);
    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);  
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE) {
      Serial.print("Pressure detected");
      break;
    }
    delay(50);
  }

  // Plot
  for (int i = 0; i < points; i++) {
    

    lose_ion(lose_probability);  // Lose an ion 
    //ions = 2;  // debug
    x = phi_min + i*step;
    y = d*(sinc2(c*(x-a)) + b0);
    if (ions == 0) y = (1+b0)*d;
    if (ions == 1) y = (0.5+b0)*d;
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 3, BLACK);
    if (ions == 2) delay(datapoint_delay);
    //}
    //else while(p.z < MINPRESSURE || p.z > MAXPRESSURE) {
    //  delay(1);
    //  if (p.z > MINPRESSURE && p.z < MAXPRESSURE) {
    //  break;
    //  }
    //}
  }

  // Fit
  for (int i = 0; i < points*modifier; i++) {
    if (ions < 2) break;
    x = phi_min + i*step/modifier;
    y = d*(sinc2(c*(x-a)) + b0);
    new_x = mapf(x, phi_min, phi_max, 0 +margin, tft.width() -margin);
    new_y = mapf(y, 0, 1, 0 +margin, tft.height() -margin);
    tft.fillCircle(new_x, new_y, 1, GREEN);
  }

  tft.setCursor(margin, tft.height() - 40);
  tft.setTextColor(BLACK);
  tft.setTextSize(3);
  if (ions < 2) {
    tft.print("Fit failed!...");
  }
  else {
    tft.print("Niceley Tickled!");
    tickle++;
  }

  while (true) {
    if ((digitalRead(30) == LOW) || (digitalRead(31) == LOW) || (digitalRead(32) == LOW) || (digitalRead(33) == LOW)) {
      return;
    }
  }
}


int selection = 0;
bool parsed = false;
bool run = false;
bool trapping = false;

void menu(void) {
  Serial.print("selection: "); Serial.println(selection);

  char prefix[8] = "       ";
  // Array of chars has one invisible element "/0" at the end!
  int prefix_length = (sizeof(prefix)/sizeof(prefix[0])) - 1;
  Serial.print("Prefix is ");
  Serial.print(prefix_length);
  Serial.print(" long\n\n");
  prefix[selection] = '>';

  tft.fillScreen(BLACK);
  tft.setRotation(1);
  //tft.fillRect(0, 60, 30, 180, RED);
  tft.setCursor(0, 0);
  tft.setTextColor(WHITE);
  tft.setTextSize(4);
  
  tft.println("Experiments:");
  tft.setTextSize(2);
  tft.println();
  tft.println();
  tft.print(prefix[0]); tft.print("  Compensation coils\n");
  tft.print(prefix[1]); tft.print("  LF COM\n");
  tft.print(prefix[2]); tft.print("  LF Rock\n");
  tft.print(prefix[3]); tft.print("  ACZS\n");
  tft.print(prefix[4]); tft.print("  Tickle Raman\n");
  tft.print(prefix[5]); tft.print("  MS Gate\n");
  tft.print(prefix[6]); tft.print("  Pulse shaper\n");
  if (game_over == true) {
    tft.println();
    tft.println();
    tft.setTextSize(4);
    uint16_t colors[] = {WHITE, RED, GREEN, BLUE, MAGENTA, YELLOW, CYAN};
    tft.setTextColor(colors[random(7)]);tft.print("C");
    tft.setTextColor(colors[random(7)]);tft.print("O");
    tft.setTextColor(colors[random(7)]);tft.print("N");
    tft.setTextColor(colors[random(7)]);tft.print("G");
    tft.setTextColor(colors[random(7)]);tft.print("R");
    tft.setTextColor(colors[random(7)]);tft.print("A");
    tft.setTextColor(colors[random(7)]);tft.print("T");
    tft.setTextColor(colors[random(7)]);tft.print("S ");
    tft.setTextColor(colors[random(7)]);tft.print("D");
    tft.setTextColor(colors[random(7)]);tft.print("r");
    tft.setTextColor(colors[random(7)]);tft.print(".");
    tft.setTextColor(colors[random(7)]);tft.print("!");
  }
  while (true) {
    if ((parsed == true) && (run == true)) {
      Serial.println("PARSE AND RUN");
      parsed = false;
      run = false;
      delay(50);
      return;
    }
    else if (digitalRead(33) == LOW) {
      selection = (selection + 1) % prefix_length;
      menu();
      return;
    }
    else if (digitalRead(32) == LOW) {
      if (selection <= 0) {
        selection = prefix_length - 1;
      }
      else {
        selection--;
      }
      menu();
      return;
    }
    else if (digitalRead(34) == LOW) {
      //selection = -1;
      trapping = true;
      parsed = false;
      run = false;
      return;
    }
    else if (digitalRead(30) == LOW) {
      parsed = true;
      run = false;
    }
    else if (digitalRead(31) == LOW) {
      run = true;
    }
  }
}

void loop() {
  Serial.print("Main loop "); Serial.println(micros());
  get_fidelity();
  print_status();
  // debug
  if (false) {
    datapoint_delay = 0; 
    lose_probability = 0;
    fidelity = 982.0;
    ions = 2;
    gate();
    return;
  }
  if (false) {
    datapoint_delay = 0; 
    lose_probability = 0;
    fidelity = 997.;
  }
  menu();
  if (trapping == true) {
    trapping = false;
    trap();
  }
  else if (selection == 0) compensation_coils();
  else if (selection == 1) raman_lf_com();
  else if (selection == 2) raman_lf_rock();
  else if (selection == 3) aczs();
  else if (selection == 4) tickle_raman();
  else if (selection == 5) gate();
  else if (selection == 6) pulse_shaper();
}