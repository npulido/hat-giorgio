# Giorgio's PhD ceremonial hat


## General

### TODO
 - Experiment list changes as the game progresses, e.g. the **pulse shaper** appears.
 - Loop checks the status / goals and calls menu.
 - Each experiment returns to the **main loop**.
 - Implement switch to turn the thing **on**.
 - **Cheat Mode:** in trapping hold **F5** for 5 seconds enters *cheat mode*
 - implement **unschedule**


### DONE
 - Exit experiments by pressing *any key*.
 - define `loose_ion()` to loose ions until 0.
 - Experiments are selected / entered via PARSE AND RUN.
 - **FIX** When trapping ions exits and menu is in this exp, it runs.
  

### Would be nice TODO, but no time
 - implement `level of difficulty` modifier
 - Make the menu iterative instead of recursive.


## MENU

### TODO
 - Do **not** draw all the menu text while selecting, just the "**>**".
   - Menu must just change `selection` and redraw **>** instead of calling itself recursively.


## General plots

### TODO
 - Save the data for next round / replot
 - Print **RED** vertical line along current datapoint
 - Delay between datapoint but not in fit.

### DONE
 - plot datapoints.
 - fail randomly due to ion loss.
 - plot **fit** in the end.


## Experiment FAIL

### TODO
 - two ions exps should fail differently for one ion than for zero ions.

### DONE
 - Loose ions randomly


## Ideas
